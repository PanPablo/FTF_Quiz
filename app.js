

const quizContainer = document.getElementById('quiz');
const resultsContainer = document.getElementById('results');
const submitButton = document.getElementById('submit');


//Refresh
var refresh = document.getElementById('refresh');

function reload () {
    location.reload();
}

refresh.onclick = function reload () {
    location.reload();
};

//Get Result Modal

var resultBox = document.getElementById("result");

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};

 var start = document.getElementById("start");

start.onclick = function() {
   start.style.display = "none";
    const myQuestions = [
        {
            no: "1",
            photo: "photo1.jpg",
            question: "Na jaką największą wysokość można wjechać wyciągiem na naszych wyjazdach?",
            answers: {
                a: "3200m.n.p.m.",
                b: "2800m.n.p.m.",
                c: "3600m.n.p.m."
            },
            correctAnswer: "c"
        },

        {
            no: "2",
            photo: "photo2.jpg",
            question: "W jakiej Alpejskiej miejscowości nie odbywały się Zimowe Igrzyska Olimpijskie ?",
            answers: {
                a: "Cortina d’Ampezzo",
                b: "Chamonix",
                c: "Val Thorens"
            },
            correctAnswer: "c"
        },
        {
            no: "3",
            photo: "photo3.jpg",
            question: "Co oznacza słowo „avalanche” ?",
            answers: {
                a: "Lawina",
                b: "Gęsta Mgła",
                c: "Ewolucja snowboardowa",
            },
            correctAnswer: "a"
        },

        {
            no: "4",
            photo: "photo4.jpg",
            question: "Jesteś świadkiem wypadku na stoku, jaka jest pierwsza czynność jaką wykonujesz ?",
            answers: {
                a: "Dzwonię po ratowników",
                b: "Oznaczam miejsce wypadku<br> skrzyżowanymi nartami",
                c: "Udzielam pierwszej pomocy",
            },
            correctAnswer: "b"
        },

        {
            no: "5",
            photo: "photo5.jpg",
            question: "Najwyżej położony ośrodek narciarski w Europie to: ",
            answers: {
                a: "Les 2 Alpes",
                b: "Krontplatz",
                c: "Val Thorens",
            },
            correctAnswer: "c"
        },

        {
            no: "6",
            photo: "photo6.jpg",
            question: "Długość skrętu narty definiuje:",
            answers: {
                a: "Długość",
                b: "Promień",
                c: "Szerokość",
            },
            correctAnswer: "b"
        },

        {
            no: "7",
            photo: "photo7.jpg",
            question: "Które z poniższych nie jest trickiem snowboardowym?",
            answers: {
                a: "Rodeo",
                b: "Backflip",
                c: "Tailwhip",
            },
            correctAnswer: "c"
        },

        {
            no: "8",
            photo: "photo8.jpg",
            question: "Angielskie słowo 'powder' oznacza śnieg:",
            answers: {
                a: "Zmrożony i twardy",
                b: "Świeży i miękki",
                c: "Mokry i topniejący",
            },
            correctAnswer: "b"
        },

        {
            no: "9",
            photo: "photo9.jpg",
            question: "Ile wyjazdów zorganizowało Feel The Flow w sezonie zimowym 18/19 ?",
            answers: {
                a: "15",
                b: "22",
                c: "42",
            },
            correctAnswer: "c"
        },

        {
            no: "10",
            photo: "photo10.jpg",
            question: "Czy pojedziesz z Feel The Flow na wyjazd ? ;))",
            answers: {
                a: "Oczywiście!",
                b: "Napewno nie!",
                c: "Nie wiem",
            },
            correctAnswer: "a"

        },

    ];


    function buildQuiz() {
        // we'll need a place to store the HTML output
        const output = [];


        // for each question...
        myQuestions.forEach((currentQuestion, questionNumber) => {
            // we'll want to store the list of answer choices
            const answers = [];


            // and for each available answer...
            for (letter in currentQuestion.answers) {
                // ...add an HTML radio button
                answers.push(
                    `<label>
             <input type="radio" name="question${questionNumber}" value="${letter}">
              ${letter} :
              ${currentQuestion.answers[letter]}
           </label>`
                );
            }

            // add this question and its answers to the output
            output.push(
                `<div class="slide">
           <img class="qphoto" src="${currentQuestion.photo}" alt="Photo">
           <div class="progres">${currentQuestion.no}/10</div>
           <div class="question"> ${currentQuestion.question} </div>
           <div class="answers"> ${answers.join("")} </div>
         </div>`
            );
        });

        // finally combine our output list into one string of HTML and put it on the page
        quizContainer.innerHTML = output.join("");
    }

    function showResults() {
        // gather answer containers from our quiz
        const answerContainers = quizContainer.querySelectorAll(".answers");

        // keep track of user's answers
        let numCorrect = 0;

        // for each question...
        myQuestions.forEach((currentQuestion, questionNumber) => {
            // find selected answer
            const answerContainer = answerContainers[questionNumber];
            const selector = `input[name=question${questionNumber}]:checked`;
            const userAnswer = (answerContainer.querySelector(selector) || {}).value;

            // if answer is correct
            if (userAnswer === currentQuestion.correctAnswer) {
                // add to the number of correct answers
                numCorrect++;

                // color the answers green
                // answerContainers[questionNumber].style.color = "lightgreen";
            } else {
                // if answer is wrong or blank
                // color the answers red
                // answerContainers[questionNumber].style.color = "red";
            }
        });

        // show number of correct answers out of total
        console.log("koniec");
        modal.style.display = "block";

        if (numCorrect == myQuestions.length) {

            resultsContainer.innerHTML = `Odpowiedziałeś poprawnie na ${numCorrect} z ${myQuestions.length} pytań!`;
            resultBox.innerHTML = `Twój wynik to: ${numCorrect} na ${myQuestions.length}! Świetnie!`;
            clearInterval(downloadTimer);
        } else if (numCorrect < 5) {
            resultsContainer.innerHTML = `Odpowiedziałeś poprawnie na ${numCorrect} z ${myQuestions.length} pytań!`;
            resultBox.innerHTML = `Twój wynik to: ${numCorrect} na ${myQuestions.length}! Nienajlepiej...`;
            clearInterval(downloadTimer);
        } else {
            resultsContainer.innerHTML = `Odpowiedziałeś poprawnie na ${numCorrect} z ${myQuestions.length} pytań!`;
            resultBox.innerHTML = `Twój wynik to: ${numCorrect} na ${myQuestions.length}! Nieźle!`;
            clearInterval(downloadTimer);
        }

        var scorefb = numCorrect;

            const container = document.getElementById('animProg');
            container.innerHTML = "";
            const score = numCorrect;

            if (score < 0 || score > 10) {
                container.innerHTML = `You can't have a score greater than 10 or less than 0.`
                inp1.value = "";
            }

            else {
                container.innerHTML = `<div id='progress'>${score}</div>`;

                let bar = {};
                const progressProps = {
                    strokeWidth: 6,
                    easing: 'easeInOut',
                    duration: 1400,
                    color: '#eee',
                    trailColor: '#eee',
                    trailWidth: 6,
                    svgStyle: null
                };

                if (score / 10 > 0.7 && score / 10 < 11) {
                    progressProps.color = '#2ECC71';
                    bar = new ProgressBar.Circle(container, progressProps);

                } else if ( score / 10 > 0.3 && score / 10 < 0.8) {
                    progressProps.color = '#FCC061';
                    bar = new ProgressBar.Circle(container, progressProps);

                } else if (score / 10 > -1 && score / 10 < 4) {
                    progressProps.color = 'red';
                    bar = new ProgressBar.Circle(container, progressProps);
                }


                bar.animate(score / 10);
            }
        }


    function showSlide(n) {
        slides[currentSlide].classList.remove("active-slide");
        slides[n].classList.add("active-slide");
        currentSlide = n;

        if (currentSlide === 0) {
            previousButton.style.display = "none";
        } else {
            previousButton.style.display = "inline-block";
        }

        if (currentSlide === slides.length - 1) {
            nextButton.style.display = "none";
            submitButton.style.display = "inline-block";
        } else {
            nextButton.style.display = "inline-block";
            submitButton.style.display = "none";
        }
    }

    function showNextSlide() {
        showSlide(currentSlide + 1);
    }

    function showPreviousSlide() {
        showSlide(currentSlide - 1);
    }

    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");

    // display quiz right away
    buildQuiz();

    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    let currentSlide = 0;

    showSlide(0);

    // on submit, show results
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);

    //Timer

    var timeleft = 120;
    var refresh = document.createElement("button");
    var downloadTimer = setInterval(function(){
        document.getElementById("progressBar").value = 120 - --timeleft;

        document.getElementById("countdowntimer").textContent = timeleft;
        if(timeleft <= 0)
            clearInterval(downloadTimer);
        console.log(timeleft);

        if (timeleft <= 0) {
            console.log('koniec');
            modal.style.display = "block";
            resultBox.innerHTML = 'Czas minał :(';
            resultBox.appendChild(refresh);
            refresh.innerHTML = "Od początku";
            refresh.onclick = function(){
                location.reload();
            };
            span.style.display = "none";
            document.getElementById('btns').style.display = "none";
            document.getElementById('bar').style.display = "none";
        }
    },1000);

    var btns = document.getElementById("btns");

    btns.style.display = "flex";

    var timeWrap = document.getElementById("timeWrap");

    timeWrap.style.display = "block";
};

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};

document.getElementById('shareBtn').onclick = function() {

    FB.ui({
        display: 'popup',
        method: 'share',
        title: 'I got ' + score + '! How about you?',
        description: 'What kind of rock are you? Find out now!',

    }, function(response){}); };









